﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonTestScript : MonoBehaviour
{
    public TextMeshProUGUI textMeshPro;
    private string lastMessage = "";

    public void AddToDebugScreen(string message)
    {
        if (lastMessage.Equals(message))
        {
            textMeshPro.text += ".";
        }
        else
        {
            textMeshPro.text = (textMeshPro.text + "\n" + message);
        }
        lastMessage = message;
    }

    public void ThankYouMessage()
    {
        textMeshPro.text = "Thank You For Your Support :)";
    }

    public void ClearDebugScreen()
    {
        textMeshPro.text = "";
    }

    private void Update()
    {
        if (Input.GetButtonUp("Cancel"))
        {
            textMeshPro.text = "Exiting...";
            Application.Quit();
        }
    }
}
