Unity Event Trigger Tester [(Google Play)](https://play.google.com/store/apps/details?id=dev.jukrapopk.eventtrigger)

Allows developers to test how Event Trigger behaves on mobile phones.

Event types:
- Pointer Enter
- Pointer Exit
- Pointer Down
- Pointer Up
- Pointer Click
- Drag
- Drop
- Scroll
- Select
- Deselect
- Move
- Initialize Potential Drag
- Begin Drag
- End Drag
- Submit
- Cancel

*Didn't include "UpdateSelected" event type.
*Created using Unity 2019.2.17f1
